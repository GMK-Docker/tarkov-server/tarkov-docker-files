FROM node:lts-bullseye
#ENV NPM_CONFIG_LOGLEVEL info
ENV sptakiver="3.8.0"
EXPOSE 6969
RUN mkdir -p /opt/SPT-AKI && cd /opt/SPT-AKI && git clone https://dev.sp-tarkov.com/SPT-AKI/Server.git
RUN cd /opt/SPT-AKI/Server && git fetch && git checkout 3.8.0
RUN cd /opt/SPT-AKI/Server && git lfs pull && git lfs ls-files 
RUN cd /opt/SPT-AKI/Server/project && npm install
RUN echo "Git version: $(git --version)"
RUN echo "Git LFS version: $(git-lfs --version)"
RUN echo "Node.js version: $(node --version)"
RUN echo "NPM version: $(npm --version)"
RUN echo "Latest Commit Hash: $(git rev-parse HEAD)"
RUN cd /opt/SPT-AKI/Server/project && npm run build:types && npm run build:bundle-release
RUN cp -r /opt/SPT-AKI/Server/project/assets /opt/SPT-AKI/Server/project/obj/
VOLUME /data
RUN ln -s /data /opt/SPT-AKI/Server/project/obj/user
WORKDIR /opt/SPT-AKI/Server/project/obj
ENTRYPOINT ["node", "Program.js"]
